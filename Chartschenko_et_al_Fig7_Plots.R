library(ggplot2)
library(tidyr)
library(ggpubr)
library(multipanelfigure)
library(magrittr)

## Panel 7B

data <- read.delim(file="PerImageData.csv", header=T, sep="\t")
summary(data)


data_sub <- subset(data, First.Protein. %in% "none")
data_sub <- subset(data_sub, ! First.Content. %in% "dsRNA-RA")

data_sub$First.Content. <- factor(data_sub$First.Content.,
                                  levels = c('control','dsRNA-RA', 'dsRNA-2'),ordered = TRUE)


timeline <- ggplot(data_sub, aes(x=as.factor(First..Metadata_Time.), y=LDl_Area_by_Cell_Area, fill=First.Content.)) + 
              geom_boxplot() + 
              scale_y_continuous(limits=c(0,0.09))+
              scale_x_discrete(labels=c("1" = "5", "2" = "10",
                                        "3" = "15", "4" = "20",
                                        "5" = "25", "6" = "30",
                                        "7" = "35", "8" = "40",
                                        "9" = "45", "10" = "50",
                                        "11" = "55", "12" = "60"))+ 
              labs(x = "time [min]", y = "Lipid storage (LD Area by Cell Area)")+
              scale_fill_manual(values=c("#999999", "white", "white"))+
              stat_compare_means(aes(group = First.Content.), label = "p.signif", size=7)+
              theme(legend.position = "none",
                    axis.text = element_text(size=14),
                    axis.title = element_text(size=16),
                    legend.text = element_text(size=14))



## Panel 7C

data <- read.delim(file="All_LDs_Data.csv", header=T, sep="\t")
summary(data)


data_sub <- subset(data, Protein %in% "none")
data_sub <- subset(data_sub, ! Content %in% "dsRNA-RA")

data_sub$Content <- factor(data_sub$Content,
                                  levels = c('control','dsRNA-RA', 'dsRNA-2'),ordered = TRUE)

timeDiameter <- ggplot(data_sub, aes(x=as.factor(Metadata_Time), y=AreaShape_MajorAxisLength, fill=Content)) + 
                  geom_boxplot() +
                  scale_fill_manual(values=c("#999999", "white", "white"))+
                  scale_x_discrete(labels=c("1" = "5", "2" = "10",
                                             "3" = "15", "4" = "20",
                                             "5" = "25", "6" = "30",
                                             "7" = "35", "8" = "40",
                                             "9" = "45", "10" = "50",
                                             "11" = "55", "12" = "60"))+ 
                  scale_y_continuous(limits = c(0,24))+
                  labs(x = "time [min]", y = "Lipid droplet diameter")+
                  stat_compare_means(aes(group = Content), label = "p.signif", size=7)+
                  theme(legend.position = "none",
                        axis.text = element_text(size=14),
                        axis.title = element_text(size=16),
                        legend.text = element_text(size=14))


## combine the plots for the figure

figure <- multi_panel_figure(columns = 4, rows = 2)

figure %<>%
  fill_panel(timeline, column = 2:4, row = 1) %<>%
  fill_panel(RNAiPlotLong, column =1, row =1:2) %<>%
  fill_panel(timeDiameter, column = 2:4, row =2)

